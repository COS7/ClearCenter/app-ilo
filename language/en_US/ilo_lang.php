<?php

$lang['ilo_app_name'] = 'iLO';
$lang['ilo_app_description'] = 'Manage iLO (Integrated Lights-Out) settings for multiple systems on your network.';
