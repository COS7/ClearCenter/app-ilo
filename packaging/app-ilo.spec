
Name: app-ilo
Epoch: 1
Version: 1.0.18
Release: 1%{dist}
Summary: iLO
License: GPLv3
Group: Applications/Apps
Packager: ClearCenter
Vendor: ClearCenter
Source: %{name}-%{version}.tar.gz
Buildarch: noarch
Requires: %{name}-core = 1:%{version}-%{release}
Requires: app-base

%description
Manage iLO (Integrated Lights-Out) settings for multiple systems on your network.

%package core
Summary: iLO - API
License: LGPLv3
Group: Applications/API
Requires: app-base-core
Requires: app-redfish-core >= 1:1.0.15
Requires: clearos-framework >= 7.5.25

%description core
Manage iLO (Integrated Lights-Out) settings for multiple systems on your network.

This package provides the core API and libraries.

%prep
%setup -q
%build

%install
mkdir -p -m 755 %{buildroot}/usr/clearos/apps/ilo
cp -r * %{buildroot}/usr/clearos/apps/ilo/

install -d -m 0755 %{buildroot}/var/clearos/ilo
install -d -m 0755 %{buildroot}/var/clearos/ilo/backup

%post
logger -p local6.notice -t installer 'app-ilo - installing'

%post core
logger -p local6.notice -t installer 'app-ilo-api - installing'

if [ $1 -eq 1 ]; then
    [ -x /usr/clearos/apps/ilo/deploy/install ] && /usr/clearos/apps/ilo/deploy/install
fi

[ -x /usr/clearos/apps/ilo/deploy/upgrade ] && /usr/clearos/apps/ilo/deploy/upgrade

exit 0

%preun
if [ $1 -eq 0 ]; then
    logger -p local6.notice -t installer 'app-ilo - uninstalling'
fi

%preun core
if [ $1 -eq 0 ]; then
    logger -p local6.notice -t installer 'app-ilo-api - uninstalling'
    [ -x /usr/clearos/apps/ilo/deploy/uninstall ] && /usr/clearos/apps/ilo/deploy/uninstall
fi

exit 0

%files
%defattr(-,root,root)
/usr/clearos/apps/ilo/controllers
/usr/clearos/apps/ilo/htdocs
/usr/clearos/apps/ilo/views

%files core
%defattr(-,root,root)
%exclude /usr/clearos/apps/ilo/packaging
%exclude /usr/clearos/apps/ilo/unify.json
%dir /usr/clearos/apps/ilo
%dir /var/clearos/ilo
%dir /var/clearos/ilo/backup
/usr/clearos/apps/ilo/deploy
/usr/clearos/apps/ilo/language
