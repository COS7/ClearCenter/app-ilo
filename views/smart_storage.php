<?php

/**
 * iLO Smart Storage view.
 *
 * @category   apps
 * @package    ilo
 * @subpackage views
 * @author     ClearCenter <developer@clearcenter.com>
 * @copyright  2018 ClearCenter
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearcenter.com/support/documentation/clearos/ilo
 */



///////////////////////////////////////////////////////////////////////////////
// Smart Storage
///////////////////////////////////////////////////////////////////////////////


foreach ($info['array_controllers'] as $key => $array_controller) {

    $anchors = [];
    $headers = [
        lang('redfish_smart_storage_id'),
        lang('redfish_smart_storage_capacity_gb'),
        lang('redfish_smart_storage_serial_number'),
        lang('redfish_smart_storage_model'),
        lang('redfish_smart_storage_media_type'),
        lang('redfish_smart_storage_media_interface_speed_mbps'),
        lang('redfish_smart_storage_media_interface_type'),
        lang('redfish_smart_storage_media_name'),
        lang('redfish_smart_storage_media_location'),
        lang('redfish_smart_storage_media_current_temprature'),
        lang('redfish_smart_storage_media_rotation_speed_rpm'),
        lang('base_status'),
    ];
    $options['no_action'] = TRUE;

    foreach ($array_controller->PhysicalDrives as $id => $drive) {
       
        $status = $drive->Status->State;

        if($drive->Status->State == 'Enabled')
            $status = $status .'/'.$drive->Status->Health; 

        $item['details'] = array(
            $drive->Id,
            $drive->CapacityGB,
            $drive->SerialNumber,
            $drive->Model,
            $drive->MediaType,
            $drive->InterfaceSpeedMbps,
            $drive->InterfaceType,
            $drive->Name,
            $drive->Location,
            $drive->CurrentTemperatureCelsius,
            $drive->RotationalSpeedRpm,
            $status
        );
        
        $items[] = $item;
    }
    
    $number = $key+1;
    $title = lang('redfish_smart_storage') .' '. $number;
    
    echo summary_table(
        $title,
        $anchors,
        $headers,
        $items,
        $options
    );
}