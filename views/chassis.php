<?php

/**
 * iLO Chassis view.
 *
 * @category   apps
 * @package    ilo
 * @subpackage views
 * @author     ClearCenter <developer@clearcenter.com>
 * @copyright  2018 ClearCenter
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 * @link       http://www.clearcenter.com/support/documentation/clearos/ilo
 */

///////////////////////////////////////////////////////////////////////////////
// Load dependencies
///////////////////////////////////////////////////////////////////////////////

$this->lang->load('base');
$this->lang->load('redfish');

$read_only = TRUE;

///////////////////////////////////////////////////////////////////////////////
// Status
///////////////////////////////////////////////////////////////////////////////

echo form_open('ilo/system/chassis/index');
echo form_header(lang('redfish_chassis'));

echo field_input('chassis_sku', $info['SKU'], lang('redfish_chassis_sku'), $read_only);
echo field_input('chassis_serial_number', $info['SerialNumber'], lang('redfish_chassis_serial_number'), $read_only);
echo field_input('chassis_status_health', $info['Status']['Health'], lang('redfish_chassis_status_health'), $read_only);

echo form_footer();
echo form_close();